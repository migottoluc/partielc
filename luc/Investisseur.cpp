//
// Created by eisti on 23/03/2020.
//

#include "Investisseur.hpp"
#include <string>

#include <iostream>
using namespace std;

void Investisseur::setNom(string n) {
    nom=n;
}

void Investisseur::setMontant(int m) {
    montant=m;
}

string Investisseur::getNom() {
    return nom;
}

int Investisseur::getMontant()  {
    return montant;
}

Investisseur::Investisseur(string n, int m) {
    nom=n;
    montant=m;
}

Investisseur::~Investisseur() {};

ostream& operator<< (ostream& out, const Investisseur & i){
    out << "Nom : " << i.nom <<" / ";
    out << i.montant << "€" << endl;
    return out;
}

istream& operator >> (istream& in, Investisseur & i){
    char c;
    if(!(in >> i.nom >> c >> i.montant) || (c != ' ')){
        in.setstate(ios::failbit);
    }
    return in;
};
