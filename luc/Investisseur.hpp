//
// Created by eisti on 23/03/2020.
//

#ifndef PARTIELC_INVESTISSEUR_H
#define PARTIELC_INVESTISSEUR_H

#include <iostream>
using namespace std;

class Investisseur {
    protected:
        string nom;
        int montant;
    public:
        void setNom(string n);
        void setMontant(int m);

        string getNom();
        int getMontant();

        Investisseur(string n,int m);
        ~Investisseur();

        friend ostream& operator << (ostream& out, const Investisseur & i);
        friend istream& operator >> (istream& in, Investisseur & i);
};


#endif //PARTIELC_INVESTISSEUR_H
