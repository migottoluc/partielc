//
// Created by eisti on 23/03/2020.
//

#ifndef PARTIELC_INVESTISSEURPROVIDENTIEL_H
#define PARTIELC_INVESTISSEURPROVIDENTIEL_H

#include <iostream>
#include "Investisseur.hpp"
using namespace std;

class InvestisseurProvidentiel : public Investisseur  {
    InvestisseurProvidentiel(string n, int m);
    ~InvestisseurProvidentiel();

    friend ostream& operator << (ostream& out, const InvestisseurProvidentiel & i);
    friend istream& operator >> (istream& in, InvestisseurProvidentiel & i);
};



#endif //PARTIELC_INVESTISSEURPROVIDENTIEL_H
