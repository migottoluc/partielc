//
// Created by eisti on 23/03/2020.
//

#include "RicheOncle.hpp"
#include <string>

#include <iostream>
using namespace std;

RicheOncle::RicheOncle(string n, int m):Investisseur(n,m){}

RicheOncle::~RicheOncle() {};

ostream& operator<< (ostream& out, const RicheOncle & i){
    out << "Nom : " << i.nom <<" / ";
    out << i.montant << "€" << endl;
    return out;
}

istream& operator >> (istream& in, RicheOncle & i){
    char c;
    if(!(in >> i.nom >> c >> i.montant) || (c != ' ')){
        in.setstate(ios::failbit);
    }
    return in;
};
