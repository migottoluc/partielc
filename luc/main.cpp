#include <vector>
#include <string>
#include <cmath>
#include <iostream>   // std::cout
#include <fstream>
#include <algorithm>
#include <iterator>


using namespace std;

#include "Investisseur.hpp"
#include "CapitalRisqueur.hpp"
#include "InvestisseurProvidentiel.hpp"
#include "RicheOncle.hpp"

int TypeInvestisseurAleatoire(){
    srand(time(NULL));
    vector<int> liste = {1,2,3};
    int x = rand() % liste.size();
    return x;
}

bool Personnel::charger(const std::string & fichier) {
    std::ifstream ifs(fichier);
    if (ifs) {
        Investisseur * env ;
        std::string nom;
        std::string montant;
        while (!ifs.eof()) {
            ifs >> nom;
            ifs >> montant;
            if (!ifs.fail()) { // si les formats sont bons et pas end-of-file
                int aleat;
                aleat = TypeInvestisseurAleatoire();
                if (aleat == 1) {
                    env = new RicheOncle(nom, montant);
                } else if (aleat == 2){
                    env = new InvestisseurProvidentiel(nom, montant);
                } else if (aleat == 3){
                    env = new CapitalRisqueur(nom,montant);
                }
                std::cout << nom << " " << montant << " " << std::endl;
                staff.push_back(env);
            }
            else {
                if (!ifs.eof()) {
                    std::cerr << "Le format n'est pas bon" << std::endl;
                    return false;
                }
            }
        }
        // trier le staff

    }
    else {
        std::cerr << "Impossible d'ouvrir le ficher" << std::endl;
        return false;
    }
}


int main(){

}