//
// Created by eisti on 23/03/2020.
//

#include "CapitalRisqueur.hpp"
#include <string>

#include <iostream>
using namespace std;

CapitalRisqueur::CapitalRisqueur(string n, int m):Investisseur(n,m) {}

CapitalRisqueur::~CapitalRisqueur() {};

ostream& operator<< (ostream& out, const CapitalRisqueur & i){
    out << "Nom : " << i.nom <<" / ";
    out << i.montant << "€" << endl;
    return out;
}

istream& operator >> (istream& in, CapitalRisqueur & i){
    char c;
    if(!(in >> i.nom >> c >> i.montant) || (c != ' ')){
        in.setstate(ios::failbit);
    }
    return in;
};