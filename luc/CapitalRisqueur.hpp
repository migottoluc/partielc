//
// Created by eisti on 23/03/2020.
//

#ifndef PARTIELC_CAPITALRISQUEUR_H
#define PARTIELC_CAPITALRISQUEUR_H

#include <iostream>
#include "Investisseur.hpp"
using namespace std;


class CapitalRisqueur : public Investisseur  {
    CapitalRisqueur(string n, int m);
    ~CapitalRisqueur();

    friend ostream& operator << (ostream& out, const CapitalRisqueur & i);
    friend istream& operator >> (istream& in, CapitalRisqueur & i);

};


#endif //PARTIELC_CAPITALRISQUEUR_H
