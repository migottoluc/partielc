//
// Created by eisti on 23/03/2020.
//

#include "InvestisseurProvidentiel.hpp"
#include <string>

#include <iostream>
using namespace std;

InvestisseurProvidentiel::InvestisseurProvidentiel(string n, int m):Investisseur(n,m){
}

InvestisseurProvidentiel::~InvestisseurProvidentiel() {};

ostream& operator<< (ostream& out, const InvestisseurProvidentiel & i){
    out << "Nom : " << i.nom <<" / ";
    out << i.montant << "€" << endl;
    return out;
}

istream& operator >> (istream& in, InvestisseurProvidentiel & i){
    char c;
    if(!(in >> i.nom >> c >> i.montant) || (c != ' ')){
        in.setstate(ios::failbit);
    }
    return in;
};