//
// Created by eisti on 23/03/2020.
//

#ifndef PARTIELC_RICHEONCLE_H
#define PARTIELC_RICHEONCLE_H

#include <iostream>
#include "Investisseur.hpp"
using namespace std;


class RicheOncle : public Investisseur  {
    RicheOncle(string n, int m);
    ~RicheOncle();

    friend ostream& operator << (ostream& out, const RicheOncle & i);
    friend istream& operator >> (istream& in, RicheOncle & i);

};



#endif //PARTIELC_RICHEONCLE_H
